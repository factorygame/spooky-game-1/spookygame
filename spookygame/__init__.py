import winsound

from factorygame.core.engine_base import GameEngine
from factorygame.core.input_base import EKeys
from spookygame.core.world import TowerWorld


class SpookyEngine(GameEngine):
    """Game engine class for a zombie apocalypse."""

    def __init__(self):
        super().__init__()

        # Set default properties.
        self._window_title = "SpookyGame"
        self._frame_rate = 90
        self._starting_world = TowerWorld

        winsound.PlaySound("spookygame/music/last_fight_dead_z_pandemic.wav",
            winsound.SND_FILENAME | winsound.SND_LOOP | winsound.SND_ASYNC)

    def setup_input_mappings(self):
        self.input_mappings.add_action_mapping(
            "MoveForward", EKeys.D)

        self.input_mappings.add_action_mapping(
            "Jump", EKeys.W)
