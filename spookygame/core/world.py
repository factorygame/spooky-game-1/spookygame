import random

from factorygame.core.blueprint import (WorldGraph, GridGismo,
                                        PolygonNode, GeomHelper, DrawnActor)
from factorygame import Loc, MathStat, FColor, Actor, GameplayUtilities
from factorygame.core.engine_base import ETickGroup
from spookygame.utils.gradient import Gradient
from spookygame.core.pawns import PlayerPawn, ZombieHorde


class TowerFloor(DrawnActor):
    """Floor of the tower that players can walk on.
    """

    def __init__(self):
        super().__init__()

        ## Points defining the floor.
        self.gradient = Gradient()
        self._create_floor()

        ## Relative scale of the floor gradient to world space.
        self.length_scale = 5000

    def _create_floor(self):
        """Construction helper to add points to the floor.
        """
        self.gradient.add_key(0, 0)
        self.gradient.add_key(200, 0.5)
        self.gradient.add_key(100, 1)

    def _should_draw(self):
        end_point = self.location + (self.length_scale, 0)
        screen_end_point = self.world.view_to_canvas(end_point)
        return screen_end_point.x > self.world.get_canvas_dim().x * 0.2

    def _draw(self):
        graph = self.world
        canvas_size = graph.get_canvas_dim()
        if canvas_size.x < 50:
            return

        points = []
        # Sample 10 points from the gradient.
        for x in range(int(canvas_size.x * 0.2), int(canvas_size.x * 0.8), canvas_size.x // 10):
            decimal_x = x / canvas_size.x
            new_point = graph.view_to_canvas(
                self.location + (decimal_x * self.length_scale, self.gradient.sample(decimal_x)))
            points.extend(new_point)

        if points:
            graph.create_line(
                points, tags=(self.unique_id))

    def begin_destroy(self):
        super().begin_destroy()
        self.world.delete(self.unique_id)

    def get_world_coords(self, gradient_bias):
        """Return the world location of sampled gradient.
        """
        return self.location + (
            gradient_bias * self.length_scale,
            self.gradient.sample(gradient_bias))


class RandomFloor(TowerFloor):
    """Has random floor points.
    """

    def _create_floor(self):
        # Change this to randomly set points.
        self.gradient.add_key(0.0, 0.0)
        self.gradient.add_key(random.random() * 200,
            MathStat.lerp(0.2, 0.3, random.random()))
        self.gradient.add_key(random.random() * 400,
            MathStat.lerp(0.4, 0.6, random.random()))
        self.gradient.add_key(random.random() * 600, 1.0)


class FloorManager(Actor):
    def __init__(self):
        super().__init__()

        ## List of floors in active use in order left to right. Old
        # floors are removed.
        self.active_floors = []

        ## Index of the floor the player is currently at. Beyond this,
        ## floors are currently visible.
        self.current_active_floor_index = 0

        self.primary_actor_tick.tick_group = ETickGroup.WORLD

    def begin_play(self):
        super().begin_play()

        # Create the first floor.
        floor = self.world.spawn_actor(RandomFloor, Loc(0, 0))
        self.world.floor = floor
        self.active_floors.append(floor)

    def tick(self, delta_time):
        # Check if we need a new floor.
        last_floor = self.active_floors[-1]
        end_point = last_floor.get_world_coords(0.6)
        end_point.y = last_floor.get_world_coords(1.0).y
        view_end, view_start = self.world.get_view_coords()

        if end_point.x < (view_end.x * 2) and len(self.active_floors) < 5:
            # Only add 1 floor per frame.
            floor = self.world.spawn_actor(RandomFloor, end_point)
            self.active_floors.append(floor)

        # Check if player passed onto a new floor.
        current_floor = self.active_floors[self.current_active_floor_index]
        current_floor_end = current_floor.get_world_coords(0.8)
        if self.world.player.location.x > current_floor_end.x:
            self.current_active_floor_index += 1
            new_floor = self.active_floors[self.current_active_floor_index]
            self.world.floor = new_floor

        # Delete old floors that aren't visible anymore.
        delete_to_index = -1        
        for i, floor in enumerate(self.active_floors):
            if floor.get_world_coords(0.8).x < view_start.x:
                delete_to_index = i
                break

        if delete_to_index >= 0:
            for i in range(delete_to_index + 1):
                floor = self.active_floors.pop(0)
                self.world.destroy_actor(floor)

            self.current_active_floor_index -= (delete_to_index + 1)


class SimpleText(DrawnActor):
    def __init__(self):
        super().__init__()
        self.text = ""

        self.primary_actor_tick.tick_group = ETickGroup.UI

    def _should_draw(self):
        return self.world.view_to_canvas(self.location).x > self.world.get_canvas_dim().x * 0.2

    def _draw(self):
        self.world.create_text(self.world.view_to_canvas(self.location),
            text=self.text, tags=(self.unique_id, "simpletext"), font=("Comic Sans MS", 24),
            fill=FColor.blue().to_hex())

    def begin_destroy(self):
        super().begin_destroy()
        self.world.delete(self.unique_id)


class TowerWorld(WorldGraph):
    def __init__(self):
        super().__init__()

        ## Currently active floor segment player is on.
        self.floor = None

        ## Manager of active floors in the world.
        self.floor_manager = None

        ## Density of the road on x-axis to appear perspective-aligned.
        self.density_x = Gradient()
        self.density_x.add_key(0.4, 0)
        self.density_x.add_key(0.001, 0.5)
        self.density_x.add_key(0.4, 1)

    def begin_play(self):
        super().begin_play()

        # Spawn the grid lines actor to show grid lines.
        self.spawn_actor(GridGismo, (0, 0))

        # my_poly = self.deferred_spawn_actor(PolygonNode, Loc(3000, 1800))
        # my_poly.vertices = list(GeomHelper.generate_reg_poly(8, radius=400.0))
        # my_poly.fill_color = FColor.cyan()
        # my_poly.total_time = 0.0

        # def my_poly_tick(delta_time):
        #     my_poly.total_time += delta_time * 0.001
        #     my_poly.vertices = list(GeomHelper.generate_reg_poly(
        #         8, radius=400.0, radial_offset=my_poly.total_time))
        #     super(PolygonNode, my_poly).tick(delta_time)
        # my_poly.tick = my_poly_tick

        # self.finish_deferred_spawn_actor(my_poly)

        # self.floor = self.spawn_actor(TowerFloor, (20, 20))
        self.floor_manager = self.spawn_actor(FloorManager, (0, 0))
        self.player = self.spawn_actor(PlayerPawn, (20, 20))
        self.zombies = self.spawn_actor(ZombieHorde, (-100, 0))

        self.control_hint = self.spawn_actor(SimpleText, Loc(1000, 600))
        self.control_hint.text = "D - Run\nW - Jump"

        self.zoom_ratio = 9

    def restart(self):
        GameplayUtilities.travel(TowerWorld)

    def view_to_canvas(self, in_coords, clamp_to_viewport=False):
        """
        Return viewport coordinates in canvas coordinates as a Loc.
        
        :param in_coords: Viewport coordinates as a Loc.

        :param clamp_to_viewport: Whether to clamp coordinates to valid
        canvas coordinates.

        :return: Canvas coordinates converted from in_coords.
        """

        canvas_dim = self.get_canvas_dim()
        tr, bl = self.get_view_coords()

        coords = super().view_to_canvas(in_coords, clamp_to_viewport)

        # Add perspective correction.
        center = canvas_dim // 2
        center_offset = coords - center

        view_percent = MathStat.getpercent(in_coords, bl, tr)

        warp_amount = 1 - self.density_x.sample(view_percent.x)
        center_offset.x *= warp_amount

        coords = center + center_offset
        coords.x = MathStat.clamp(
            coords.x, canvas_dim.x * 0.2, canvas_dim.x * 0.8)

        return coords

    # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Ignore editor mode viewport interactions.

    def on_graph_wheel_input(self, event):
        pass

    def on_graph_motion_input(self, event):
        pass
