from tkinter.ttk import Button

from factorygame import Loc, FColor, GameplayStatics, MathStat
from factorygame.core.engine_base import ETickGroup
from factorygame.core.blueprint import PolygonNode, GeomHelper
from factorygame.core.input_base import EInputEvent
from spookygame.utils.movement import JumpComponent


class PlayerPawn(PolygonNode):
    """Player that runs up the tower.
    """

    def __init__(self):
        super().__init__()

        ## Width of player model, in world units.
        self.width = 20

        ## Target of movement along gradient per second in world units.
        self.move_target = 0.0

        self.gradient_bias = 0.2

        self._last_floor = None

        ## Whether we are dead (stop movement!)
        self.is_dead = False

        ## How long we survived so far.
        self.survived_time = 0.0

    def begin_play(self):
        super().begin_play()
        octagon = GeomHelper.generate_reg_poly(
            8, radius=self.width * 2, center=Loc(0, self.width))
        allowed_verts = {2, 3, 6, 7}
        self.vertices = list(map(lambda x: x[1], filter(
            lambda x: x[0] in allowed_verts, enumerate(octagon))))

        self.fill_color = FColor(240, 120, 29)  # Light orange

        input_comp = GameplayStatics.game_engine.input_mappings
        self.setup_player_input_component(input_comp)

        self.jump_component = JumpComponent()
        self.jump_component.jump_apex = 200
        self.jump_component.duration = 0.35

    def tick(self, delta_time):
        if self.is_dead: return super().tick(delta_time)

        self.jump_component.tick_component(delta_time)

        delta_seconds = delta_time * 0.001
        self.survived_time += delta_seconds

        floor = self.world.floor
        if self._last_floor is None:
            self._last_floor = floor
        elif self._last_floor is not floor:
            # We got a new floor this frame.
            self.gradient_bias = 0.2
            self._last_floor = floor

        self.gradient_bias += self.move_target * delta_seconds
        self.location = floor.get_world_coords(self.gradient_bias)
        # Hover player above floor to adjust for box vertices.
        self.location.y += self.jump_component.offset + 20

        # Align world viewport to center player model.
        self.world._view_offset = self.location

        super().tick(delta_time)

    def setup_player_input_component(self, input_component):
        """Bind functions to input events.
        """
        input_component.bind_action(
            "MoveForward", EInputEvent.PRESSED, self.on_start_move_forward)
        input_component.bind_action(
            "MoveForward", EInputEvent.RELEASED, self.on_stop_move_forward)

        input_component.bind_action(
            "Jump", EInputEvent.PRESSED, self.on_start_jump)

    def on_start_move_forward(self):
        self.move_target = 0.3

    def on_stop_move_forward(self):
        self.move_target = 0

    def on_start_jump(self):
        self.jump_component.jump()


class ZombieHorde(PolygonNode):
    """A horde of zombies, rendered as a single block.
    """

    def __init__(self):
        super().__init__()
        ## Rotation of vertices, in radians.
        self.rotation = 0.0

        self.num_sides = 10

        self.radius = 500.0

        self.primary_actor_tick.tick_group = ETickGroup.UI

        self.scale_direction = 1

    def begin_play(self):
        super().begin_play()
        verts = GeomHelper.generate_reg_poly(self.num_sides, radius=self.radius)
        self.vertices = list(verts)

        self.fill_color = FColor(153, 208, 102)  # Light green

    def tick(self, delta_time):
        self._move(delta_time)
        if not self.world.player.is_dead:
            self._check_collision(delta_time)

        super().tick(delta_time)

    def _move(self, delta_time):
        """Move towards player.
        """
        # Move towards player.
        target_direction = self.world.player.location - self.location
        target_direction /= abs(target_direction)  # To unit vector.

        target_location = self.location + (target_direction * delta_time * 3.7)

        self.location = MathStat.lerp(self.location, target_location, 0.4)

        # Rotate a bit.
        self.rotation += delta_time * 0.002

        # Scale up/down a bit.
        self.radius += 0.5 * delta_time * self.scale_direction
        if self.radius < 400 or self.radius > 600:
            self.scale_direction *= -1

        self.vertices = list(GeomHelper.generate_reg_poly(
            self.num_sides, radius=self.radius, radial_offset=self.rotation))

    def _check_collision(self, delta_time):
        """Check if we hit the player to eat his brains.
        """
        # Assume we are a circle.
        dist_to_player = MathStat.getdist(self.location, self.world.player.location)
        if dist_to_player < self.radius:
            # A collision occured.
            self.world.player.is_dead = True

            self.world.create_text(Loc(50, 10),
                text="You Were Eaten By A Zombie Horde!\n"
                "Survival Time: %.2f seconds" % self.world.player.survived_time,
                font=("Comic Sans MS", 24), anchor="nw",
                fill=FColor.red().to_hex())

            restart_btn = Button(self.world, text="Restart",
                command=self.world.restart)
            self.world.create_window(Loc(50, 110), window=restart_btn,
                anchor="nw")

            self.world.destroy_actor(self.world.control_hint)


