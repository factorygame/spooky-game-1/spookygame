from factorygame import MathStat
from spookygame.utils.gradient import Gradient


class JumpComponent:
    """Helper to allow player jumping.
    """

    def __init__(self):

        ## Maximum height offset reached by jumping.
        self.jump_apex = 200

        ## Current location of jump (vertical only).
        self.offset = 0

        ## Duration in seconds of a jump.
        self.duration = 1.0

        ## Whether we are currently mid-jump.
        self._is_jumping = False

        ## Curve to jump between [0, 1].
        self._height_curve = Gradient()
        self._height_curve.add_key(0.4, 0)
        self._height_curve.add_key(1, 0.4)
        self._height_curve.add_key(0, 0.8)

        ## Current position on the jump curve beteen [0, 1].
        self._curve_bias = 0.0

    def jump(self):
        """Start a jump, only if not already jumping.
        """
        if self.is_jumping:
            return
        self._is_jumping = True

    def tick_component(self, delta_time):
        """Update the jump location offset.

        MUST BE CALLED MANUALLY EVERY FRAME!
        """
        if not self.is_jumping:
            return

        delta_seconds = delta_time * 0.001
        curve_advancement = delta_seconds / self.duration
        self._curve_bias += curve_advancement

        new_offset = self._height_curve.sample(self._curve_bias) * self.jump_apex
        self.offset = MathStat.lerp(self.offset, new_offset, 0.4)

        if self._curve_bias > 1:
            self._is_jumping = False
            self._curve_bias = 0.0

    @property
    def is_jumping(self):
        return self._is_jumping
