# Spooky Game 1

Python game with spooky zombie chase.

## Project Overview

In the midst of a zombie apocalypse, the player has woken up at the base of 
mysterious tower.

Zombies chase the player in a horde that increases and decreases in size
rythmically. The player must run and jump up the tower to proceed. If the
zombies manage to catch up to the player, he is eaten. A high score is
achieved by surviving the longest time.
